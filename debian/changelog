freehep-xml (2.1.2+dfsg1-6) unstable; urgency=medium

  * Team upload.

  [ Philipp Huebner ]
  * Remove myself from uploaders

  [ Andreas Tille ]
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Secure URI in copyright format (routine-update)
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 16 Jan 2025 12:05:19 +0100

freehep-xml (2.1.2+dfsg1-5) unstable; urgency=medium

  [ tony mancill ]
  * Team upload.
  * Remove Gabriele Giacone from Uploaders (Closes: #856755)

  [ Giovanni Mascellani ]
  * Bump Standards-Version to 4.1.1.
  * Change priority to optional.
  * Switch build system to debhelper 10.
  * Reformat some fields in debian/control.

 -- Giovanni Mascellani <gio@debian.org>  Sat, 04 Nov 2017 16:27:29 +0100

freehep-xml (2.1.2+dfsg1-4) unstable; urgency=low

  [ Giovanni Mascellani ]
  * Add real watch file.
  * Fix my email address.
  * Remove obsolete lines from gbp.conf.
  * Move package to priority extra.
  * Use secure Vcs-* URLs.
  * Bump Standards-Version to 3.9.8 (no changes required).
  * Build with debhelper 9 instead of CDBS.
  * Quit using topgit and remove leftovers.
  * Remove trivial information in debian/README.source.
  * Add classpath to the JAR manifest.
  * Make debian/copyright standards compliant.
  * Make patches DEP-3 compliant.

  [ Philipp Huebner ]
  * Added myself as uploader
  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Updated VCS links
  * Switched to source format 3.0 (quilt)
  * Switched to Debhelper 9
  * Updated debian/copyright and made it lintian clean
  * Dropped Depends on jre

 -- Giovanni Mascellani <gio@debian.org>  Thu, 21 Jul 2016 12:10:21 +0200

freehep-xml (2.1.2+dfsg1-3) unstable; urgency=low

  * Moved default-jdk from Build-Depends-Indep to Build-Depends

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Wed, 03 Mar 2010 23:13:23 +0100

freehep-xml (2.1.2+dfsg1-2) unstable; urgency=low

  * Removing unneeded patch for jdom.

 -- Giovanni Mascellani <mascellani@poisson.phc.unipi.it>  Sat, 27 Feb 2010 16:29:11 +0100

freehep-xml (2.1.2+dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #564962)

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 27 Feb 2010 12:32:13 +0100
